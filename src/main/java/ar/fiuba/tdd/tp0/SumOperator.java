package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class SumOperator implements ICommand {

	@Override
	public void execute(Stack<Float> params) {
		Float sum=(float) 0;		
		while (params.size()>1)
			sum=sum+params.pop();
		params.push(sum+params.pop());
	}

}
