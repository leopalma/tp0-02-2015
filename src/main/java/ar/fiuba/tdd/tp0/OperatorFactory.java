package ar.fiuba.tdd.tp0;

import java.util.HashMap;
import java.util.Map;

public class OperatorFactory {

	public static Map<String,ICommand> makeOperatorMap(){
		Map<String,ICommand> operators = new HashMap<String, ICommand>();
		//const operator symbols
		final String addSymbol = "+";
    	final String substractSymbol = "-";
    	final String multiplySymbol = "*";
    	final String divisionSymbol = "/";
    	final String modSymbol = "MOD";
    	final String sumSymbol = "++";
    	final String multiSubstractSymbol = "--";
    	final String multiMultiplySymbol = "**";
    	final String multiDivisionSymbol = "//";    
    	
		operators.put(addSymbol, new AddOperator());	
		operators.put(substractSymbol, new SubstractOperator());	
		operators.put(multiplySymbol, new MultiplyOperator());	
		operators.put(divisionSymbol, new DivisionOperator());	
		operators.put(modSymbol, new ModOperator());	
		operators.put(sumSymbol, new SumOperator());	
		operators.put(multiSubstractSymbol, new MultiSubstractionOperator());	
		operators.put(multiMultiplySymbol, new MultiMultiplyOperator());
		operators.put(multiDivisionSymbol, new MultiDivisionOperator());
		
		return operators;
	}
	
}
