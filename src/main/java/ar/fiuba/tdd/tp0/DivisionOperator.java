package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class DivisionOperator implements ICommand {

	@Override
	public void execute(Stack<Float> params) {
		try{
			params.push((1/params.pop())*params.pop());	
		}
		catch(Exception e){
			throw new IllegalArgumentException();
		}
		
	}

}
