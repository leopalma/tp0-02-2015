package ar.fiuba.tdd.tp0;

import java.util.Stack;

public interface ICommand {

	public void execute(Stack<Float> params);
	
}
