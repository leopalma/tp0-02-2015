package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class ModOperator implements ICommand {

	@Override
	public void execute(Stack<Float> params) {
		try{
			Float lastParam = params.pop();
			params.push(params.pop()%lastParam);
		}
		catch(Exception e){
			throw new IllegalArgumentException();
		}
	}

}
