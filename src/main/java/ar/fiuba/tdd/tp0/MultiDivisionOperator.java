package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class MultiDivisionOperator implements ICommand {

	@Override
	public void execute(Stack<Float> params) {
		Float temporalMult=(float) 1;
		while (params.size()>1)
				temporalMult*=(1/params.pop());
		params.push(params.pop()*temporalMult);
	}

}
