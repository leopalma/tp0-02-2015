package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class MultiSubstractionOperator implements ICommand {

	@Override
	public void execute(Stack<Float> params) {
		Float sumOfSubstractors=(float) 0;
		while (params.size()>1)
			sumOfSubstractors+=params.pop();
		params.push(params.pop()-sumOfSubstractors);
	}

}
