package ar.fiuba.tdd.tp0;

import java.util.Map;
import java.util.Stack;

public class RPNCalculator {

	private Map<String,ICommand> operators;
	
	public RPNCalculator() {
		operators = OperatorFactory.makeOperatorMap();
	}
	
    public float eval(String expression) {
    	Stack<Float> params = new Stack<Float>();
        String[] parsedParams;
        try{
        	parsedParams= expression.split(" ");
        }
    	catch(Exception ex){        		
			throw new IllegalArgumentException();
		}        
        for(String param : parsedParams){
        	try{
        		params.push(Float.parseFloat(param));
        	}
        	catch (NumberFormatException e){
        		try{
        			operators.get(param).execute(params);
        		}
        		catch(Exception ex){        		
        			throw new IllegalArgumentException();
        		}
        	}
        }
		return params.pop();
    }
        
    private void addOperator(String symbol, ICommand operator){
    	operators.put(symbol, operator);
    }

}
