package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class AddOperator implements ICommand {

	@Override
	public void execute(Stack<Float> params) {
		try{
			params.push(params.pop()+params.pop());
		}
		catch(Exception e){
			throw new IllegalArgumentException();
		}
	}

}
